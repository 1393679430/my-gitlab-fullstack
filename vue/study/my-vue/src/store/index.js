import Vue from 'vue'
import Vuex from 'vuex'
import cachePlugin from '../plugins/cachePlugin'
import user from './user'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  plugins: [cachePlugin],
  modules: {
    user
  }
})
