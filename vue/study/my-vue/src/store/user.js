export default{
  namespaced: true,
  state: {
    isLogin: false,
    username: ''
  },
  mutations: {
    login(state, payload = {}) {
      state.isLogin = true
      if ('username' in payload) {
        state.username = payload.username
      }
    },
    logout(state) {
      state.isLogin = false
    },
    setUserName(state, payload) {
      state.username = payload.username
    }
  },
  getters: {
    welcome: state => {
      return state.username + ', 欢迎回来！'
    }
  },
  actions: {
    login2({ commit }, payload) {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          if (payload.username === 'admin') {
            commit('login')
            commit('setUserName', payload)
            resolve()
          } else {
            reject()
          }
        }, 1000)
      })
    }
  },
  modules: {
  }
}
