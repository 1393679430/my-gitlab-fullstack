export default store => {
    if(localStorage) {
        const user = JSON.parse(localStorage.getItem('user'))
        if (user && user.isLogin) {
            store.commit('user/login')
            store.commit('user/setUserName', {username: user.username})
        }
    }

    store.subscribe((mutation, state) => {
        console.log(mutation, state, '---cachePlugin---');
        if (mutation.type.startsWith('user/')) {
            localStorage.setItem('user', JSON.stringify(state.user))
        } else if (mutation.type === 'user/logout') {
            localStorage.removeItem('user')
        }
    })
}